<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Product;
use Validator;

class DashController extends Controller
{
    public function profile()
    {
        if(Auth::check()) {
            $user = Auth::user();
            $products = Product::where('user_id', $user->id)->get();
            return view('profile', ['user' => $user, 'products' => $products]);
        }
    }
    public function create()
    {
        if(Auth::check()) {
            return view('addproduct');
        }
        return view('create');
    }
    public function store(Request $request)
    {
        if(Auth::check()) {
            $this->validate(request(), [
                'name' => 'required',
                'quantity' => 'required',
                'price' => 'required'
            ]);
            $file = $request->file('image');
            $images = "";
            //declare validation rules
            $rules = array(
                'file' => 'required|mimes:png,gif,jpeg'
            );
            $validator = Validator::make(array('file' => $file), $rules);
            if ($validator->passes()) {
                $destinationPath = 'uploads/' . microtime(true);
                $filename = $file->getClientOriginalName();
                $images = $destinationPath . '/' . $filename;
                $upload_success = $file->move($destinationPath, $filename);
            } else {
                session()->flash('message', 'Upload failed');
                return redirect()->route('dashboard')->with('success', 'Product added');
            }

            $product = Product::create([
                'name' => request('name'),
                'quantity' => request('quantity'),
                'price' => request('price'),
                'user_id' => Auth::User()->id,
                'image' => $images
            ]);
            if(!$product){
                return redirect()->back()->with('errors', 'Product not added');
            }
            return redirect()->back()->with('success', 'Added');
        }
        return view('create');
    }
    public function logout()
    {
        auth()->logout();
        return redirect('/');
    }
}
