<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\User;
use Auth;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    public function base()
    {
        $products = Product::where('status', 'APPROVED')->get();
        return view('base', ['products'=>$products]);
    }

    public function create()
    {
        return view('create');
    }

    public function store()
    {
        $this->validate(request(),[
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
        ]);
        $user = User::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => bcrypt(request('password'))
        ]);
        if(!$user){
            return redirect()->back()->with('errors', 'Registration failed');
        }

        return redirect()->back()->with('success', 'Registration successful');
    }

    public function login()
    {
        if(!auth()->attempt(request(['email', 'password']))){
            return back()->withErrors([
                'errors' => 'Please check details and login again'
            ]);
        }
        return redirect()->route('dashboard')->with('success', 'Login successful');
    }
}
