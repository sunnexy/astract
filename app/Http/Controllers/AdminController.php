<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Product;
use Carbon\Carbon;

class AdminController extends Controller
{
    public function all()
    {
        if(Auth::check()) {
            $user = Auth::user();
            $products = Product::where('status', 'PENDING')->get();
            return view('allproducts', ['user' => $user, 'products' => $products]);
        }
        return view('create');
    }
    public function approved()
    {
        if(Auth::check()) {
            $user = Auth::user();
            $products = Product::where('status', 'APPROVED')->get();
            return view('approved',['user'=>$user, 'products' => $products]);
        }
        return view('create');
    }
    public function update($id)
    {
        if(Auth::check()) {
            $product = Product::where('id', $id)->update(['status' => 'APPROVED']);
            return back();
        }
        return view('create');
    }
    public function delete($id)
    {
        if(Auth::check()) {
            $product = Product::where('id', $id)->delete();
            return back();
        }
        return view('create');
    }
}
