<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = [
        'name', 'quantity', 'price', 'image', 'user_id'
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
