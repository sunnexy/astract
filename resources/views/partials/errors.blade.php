@if(isset($errors)&&count($errors)>0)
    <div class="alert alert-dismissable alert-success" style="background-color: #4169E1;">
        <button type="button" class="close" data-dismiss="alert" aria-label="close">
            <span aria-hidden="true">&times;</span>
        </button>
        @foreach($errors->all() as $error)
            <li><strong style="color: black;">{{ $error }}</strong></li>
        @endforeach
    </div>
@endif