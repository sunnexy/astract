<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Login | E-Shopper</title>
    <link href="{{asset('public/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('public/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('public/css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{ asset('public/css/price-range.css')}}" rel="stylesheet">
    <link href="{{asset('public/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('public/css/main.css')}}" rel="stylesheet">
    <link href="{{asset('public/css/responsive.css')}}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="{{asset('public/js/html5shiv.js')}}"></script>
    <script src="{{asset('public/js/respond.min.js')}}"></script>
    <![endif]-->
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->
<section id="form"><!--form-->
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <a href="{{URL('/profile')}}">back</a>
                @include('partials.errors')
                @include('partials.success')
                <div class="signup-form"><!--sign up form-->
                    <h2>Add new product</h2>
                    <form action="{{URL('/add')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="text" placeholder="Product Name" name="name"/>
                        <input type="text" placeholder="Quantity" name="quantity"/>
                        <input type="text" placeholder="Price" name="price"/>
                        <input type="file" name="image"/>
                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}" />
                        <button type="submit" class="btn btn-default">Add</button>
                    </form>
                </div><!--/sign up form-->
            </div>
        </div>
    </div>
</section><!--/form-->