<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@base');
Route::get('/signin', 'HomeController@create')->name('login');
Route::post('/register', 'HomeController@store');
Route::post('/login', 'HomeController@login');

Route::middleware(['auth'])->group(function () {
    Route::get('/logout', 'DashController@logout');
    Route::get('/profile', 'DashController@profile')->name('dashboard');
    Route::get('/addproduct', 'DashController@create');
    Route::post('/add', 'DashController@store');
    Route::get('/allproducts', 'AdminController@all');
    Route::get('/product/approve/{id}', 'AdminController@update');
    Route::get('/product/reject/{id}', 'AdminController@delete');
    Route::get('/approved', 'AdminController@approved');
});

